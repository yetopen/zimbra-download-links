# Zimbra download links

Links as found on the [OSE download page](https://www.zimbra.com/downloads/zimbra-collaboration-open-source/) 
and [NE](https://www.zimbra.com/downloads/zimbra-collaboration/).


## 8.8.15

[Release notes](https://wiki.zimbra.com/wiki/Zimbra_Releases/8.8.15)

* RHEL/CentOS/Oracle 6: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3869.RHEL6_64.20190918004220.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.RHEL6_64.20190918004220.tgz)
* RHEL/CentOS/Oracle 7: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3869.RHEL7_64.20190918004220.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.RHEL7_64.20190918004220.tgz)
* RHEL/CentOS/Oracle 8: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3953.RHEL8_64.20200629025823.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.RHEL7_64.20190918004220.tgz)
* Rocky Linux 8: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_4362.RHEL8_64.20220721104405.tgz)
    | [NE](https://files.zimbra.com/downloads/9.0.0_GA/zcs-NETWORK-9.0.0_GA_4325.RHEL8_64.20220629074359.tgz)
* Ubuntu14: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3869.UBUNTU14_64.20190918004220.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.UBUNTU14_64.20190918004220.tgz)
* Ubuntu16: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3869.UBUNTU16_64.20190918004220.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.UBUNTU16_64.20190918004220.tgz)
* Ubuntu18: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_3869.UBUNTU18_64.20190918004220.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-NETWORK-8.8.15_GA_3869.UBUNTU18_64.20190918004220.tgz)
* Ubuntu20: [OSE](https://files.zimbra.com/downloads/8.8.15_GA/zcs-8.8.15_GA_4179.UBUNTU20_64.20211118033954.tgz)
    | [NE](https://files.zimbra.com/downloads/9.0.0_GA/zcs-NETWORK-9.0.0_GA_4178.UBUNTU20_64.20211112031526.tgz)

## 8.8.12 Isaac Newton

[Release notes](https://wiki.zimbra.com/wiki/Zimbra_Releases/8.8.12)

* RHEL/CentOS/Oracle 6: [OSE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-8.8.12_GA_3794.RHEL6_64.20190329045002.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-NETWORK-8.8.12_GA_3794.RHEL6_64.20190329045002.tgz)
* RHEL/CentOS/Oracle 7: [OSE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-8.8.12_GA_3794.RHEL7_64.20190329045002.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-NETWORK-8.8.12_GA_3794.RHEL7_64.20190329045002.tgz)
* Ubuntu14: [OSE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-8.8.12_GA_3794.UBUNTU14_64.20190329045002.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-NETWORK-8.8.12_GA_3794.UBUNTU14_64.20190329045002.tgz)
* Ubuntu16: [OSE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-8.8.12_GA_3794.UBUNTU16_64.20190329045002.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-NETWORK-8.8.12_GA_3794.UBUNTU16_64.20190329045002.tgz)
* Ubuntu18 (beta): [OSE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-8.8.12_GA_3794.UBUNTU18_64.20190329045002.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.12_GA/zcs-NETWORK-8.8.12_GA_3794.UBUNTU18_64.20190329045002.tgz)

## 8.8.11 Homi Bhabha

[Release notes](https://wiki.zimbra.com/wiki/Zimbra_Releases/8.8.11)

* RHEL/CentOS/Oracle 6: [OSE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.RHEL6_64.20181207111719.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-NETWORK-8.8.11_GA_3737.RHEL6_64.20181207111719.tgz)
* RHEL/CentOS/Oracle 7: [OSE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-NETWORK-8.8.11_GA_3737.RHEL7_64.20181207111719.tgz)
* Ubuntu14: [OSE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.UBUNTU14_64.20181207111719.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-NETWORK-8.8.11_GA_3737.UBUNTU14_64.20181207111719.tgz)
* Ubuntu16: [OSE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-8.8.11_GA_3737.UBUNTU16_64.20181207111719.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.11_GA/zcs-NETWORK-8.8.11_GA_3737.UBUNTU16_64.20181207111719.tgz)

## 8.8.10 Konrad Zuse

[Release notes](https://wiki.zimbra.com/wiki/Zimbra_Releases/8.8.10)

* RHEL/CentOS/Oracle 6: [OSE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-8.8.10_GA_3039.RHEL6_64.20180928094617.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-NETWORK-8.8.10_GA_3039.RHEL6_64.20180928094617.tgz)
* RHEL/CentOS/Oracle 7: [OSE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-8.8.10_GA_3039.RHEL7_64.20180928094617.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-NETWORK-8.8.10_GA_3039.RHEL7_64.20180928094617.tgz)
* Ubuntu14: [OSE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-8.8.10_GA_3039.UBUNTU14_64.20180928094617.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-NETWORK-8.8.10_GA_3039.UBUNTU14_64.20180928094617.tgz)
* Ubuntu16: [OSE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-8.8.10_GA_3039.UBUNTU16_64.20180928094617.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.10_GA/zcs-NETWORK-8.8.10_GA_3039.UBUNTU16_64.20180928094617.tgz)

## 8.8.9 

[Release notes](https://wiki.zimbra.com/wiki/Zimbra_Releases/8.8.9)

* RHEL/CentOS/Oracle 6: [OSE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-8.8.9_GA_2055.RHEL6_64.20180703080917.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-NETWORK-8.8.9_GA_2055.RHEL6_64.20180703080917.tgz)
* RHEL/CentOS/Oracle 7: [OSE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-8.8.9_GA_2055.RHEL7_64.20180703080917.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-NETWORK-8.8.9_GA_2055.RHEL7_64.20180703080917.tgz)
* Ubuntu14: [OSE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-8.8.9_GA_2055.UBUNTU14_64.20180703080917.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-NETWORK-8.8.9_GA_2055.UBUNTU14_64.20180703080917.tgz)
* Ubuntu16: [OSE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-8.8.9_GA_2055.UBUNTU16_64.20180703080917.tgz) 
    | [NE](https://files.zimbra.com/downloads/8.8.9_GA/zcs-NETWORK-8.8.9_GA_2055.UBUNTU16_64.20180703080917.tgz)

# Alternative mirror

[Brando Beaumont](https://lists.zetalliance.org/pipermail/users_lists.zetalliance.org/2018-April/001133.html) runs a *private* mirror 
of Zimbra install packages, you can find them [here](http://zimbramirror.itaserv.net/)

